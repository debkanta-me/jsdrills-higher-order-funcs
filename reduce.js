function reduce(elements, cb, startingValue) {
    if(typeof cb != "function") return [];
    if(typeof elements != "object") return [];
    
    let isPresent = true;
    
    if(typeof startingValue == "undefined"){
       isPresent = false;
       startingValue = elements [0]; 
    }

    for(let i= isPresent? 0: 1;i<elements.length;i++){
        startingValue = cb(startingValue, elements[i], elements);
    }
 
    return startingValue;
}

module.exports = reduce;