const flatten = require("../flatten")

const arrays = [1, [2], [[3]], [[[4]]]];

const result = flatten(arrays);

console.log(result);

