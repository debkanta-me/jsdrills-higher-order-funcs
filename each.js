function each(elements, cb) {
    if(typeof cb != "function") return [];
    if(typeof elements != "object") return [];

    for(let i=0;i<elements.length;i++){
        cb(elements[i], i);
    }
}

module.exports = each;