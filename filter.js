function filter(elements, cb) {
    if(typeof cb != "function") return [];
    if(!Array.isArray(elements)) return [];

    let clone = [];

    for(let i=0;i<elements.length;i++){
        if(cb(elements[i], i)){
            clone.push(elements[i]);
        }
    }

    return clone;
}

module.exports = filter;