function find(elements, cb) {
    if(typeof cb != "function") return [];
    if(typeof elements != "object") return [];

    for(let i=0;i<elements.length;i++){
        if(cb(elements[i], i)){
            return elements[i];
        }
    }

    return undefined;
}

module.exports = find;