function map(elements, cb) {
    if(typeof cb != "function") return [];
    if(typeof elements != "object") return [];

    let clone = [];

    for(let i=0;i<elements.length;i++){
        const foo = cb(elements[i], i, elements);
        clone.push(foo);
    }

    return clone;
}

module.exports = map;