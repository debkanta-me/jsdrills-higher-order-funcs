function elem(element, array) {
  if (typeof element == "number") {
    array.unshift(element);
    return;
  }
  element = element.pop();
  elem(element, array);
}
function flatten(elements) {
  if (!Array.isArray(elements)) return [];
  let array = [];
  while (elements.length > 0) {
    elem(elements.pop(), array);
  }

  return array;
}

module.exports = flatten;